import flask
import mammoth

app = flask.Flask("__main__")


@app.route("/")
@app.route("/home")
@app.route("/products")
def my_index():
    return flask.render_template("index.html", token="To jest info z backendu")


@app.route("/news")
def news():
    return flask.render_template("news.html")


@app.route("/basket")
def basket():
    pass


app.run(debug=True)
