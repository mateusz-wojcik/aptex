import React from 'react';
import './Menu.css';

function Menu() {
    return (
        <div className="Menu">
            <div><a href="/home">APTEX</a></div>
            <div><a href="/products">Produkty</a></div>
            <div><a href="/popular">Popularne</a></div>
            <div><a href="/news">Nowości</a></div>
            <div><a href="/basket">Koszyk</a></div>
        </div>
    );
}

export default Menu;
