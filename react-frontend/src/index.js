import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Menu from "./Menu";
import CompleteBoard from "./Board";

ReactDOM.render(<Menu/>, document.getElementById('menu'));
ReactDOM.render(<CompleteBoard/>, document.getElementById('board'));

serviceWorker.unregister();
