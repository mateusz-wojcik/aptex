import React from 'react';
import ReactTooltip from 'react-tooltip'
import './Component.css'

class ToolTip extends React.Component {
    modes = {
        green: this.getGreen()  ,
        orange: this.getOrange(),
        red: this.getRed(),
        gray: this.getGray()
    };

    constructor(props) {
        super(props);
    }

    getGreen() {
        return (
            <div className='options'>
                <div>Zmień kategorię</div>
                <div>Ignoruj</div>
            </div>
        )
    }

    getOrange() {
        return (
            <div className='options'>
                <div>Zmień kategorię</div>
                <div>Zmień klasyfikację</div>
                <div>30000</div>
                <div>Ignoruj</div>
            </div>
        )
    }

    getRed() {
        return (
            <div className='options'>
                <div>Zmień kategorię</div>
                <div>30000</div>
                <div>Ignoruj</div>
            </div>
        )
    }

    getGray() {
        return (
            <div className='options'>
                <div>Zmień kategorię</div>
                <div>30000</div>
                <div>Ignoruj</div>
            </div>
        )
    }

    render() {
        return (
            <ReactTooltip id={`${this.props.id}`} aria-haspopup='true' delayHide={500} effect='solid'
                          className={`tooltip-${this.props.mode} tooltip`}>
                {this.modes[this.props.mode]}
            </ReactTooltip>
        );
    }
}

export default ToolTip;