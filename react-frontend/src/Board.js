import React from 'react';
import logo from './logo.svg'
import './Board.css'
import ToolTip from "./Component/ToolTip";

class Filter extends React.Component {
    render() {
        return (
            <div className="Filter">
                <h3>Kategorie</h3>
                <ul className="Filter-list">
                    <li>
                        <input id="products_1" type="checkbox" name="products" value="1"/>
                        <label htmlFor="products_1">Przeciwbólowe</label>
                    </li>
                    <li>
                        <input id="products_2" type="checkbox" name="products" value="2"/>
                        <label htmlFor="products_2">Dla dzieci</label>
                    </li>
                    <li>
                        <input id="products_3" type="checkbox" name="products" value="3"/>
                        <label htmlFor="products_3">Ból gardła</label>
                    </li>
                    <li>
                        <input id="products_4" type="checkbox" name="products" value="4"/>
                        <label htmlFor="products_4">Higiena</label>
                    </li>
                    <li>
                        <input id="products_5" type="checkbox" name="products" value="5"/>
                        <label htmlFor="products_5">Dla sportowców</label>
                    </li>
                    <li>
                        <input id="products_6" type="checkbox" name="products" value="6"/>
                        <label htmlFor="products_6">Witaminy</label>
                    </li>
                </ul>
            </div>
        );
    }
}

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: props.products
        }
    }


    renderProduct(productId) {
        return <Product id={productId} mode={'orange'}/>
    }

    render() {
        return (
            <div className="Board">
                <div className="Board-row"> {this.renderProduct(0)} {this.renderProduct(1)} {this.renderProduct(2)} </div>
                <div className="Board-row"> {this.renderProduct(3)} {this.renderProduct(4)} {this.renderProduct(5)} </div>
                <div className="Board-row"> {this.renderProduct(6)} {this.renderProduct(7)} {this.renderProduct(8)} </div>
            </div>
        );
    }
}

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.id = props.id;
        this.state = {
            quantity: 0
        };
    }

    onQuantityButtonClick(mode) {
        this.setState(prevState => {
            return {quantity: mode === 'add' ? prevState.quantity + 1 : prevState.quantity - 1}
        });
    }

    onBasketAddButtonClick() {

    }

    render() {
        return (
            <div className="Product" data-tip data-for={this.id}>
                <img src={logo} alt={logo} className="Product-img"/>
                <div className="Product-description-div">
                    <p className="Product-name">Apap</p>
                    <p className="Product-qty">60 tabletek</p>
                </div>
                <div className="Product-quantity-div">
                    <button onClick={this.onQuantityButtonClick.bind(this, 'sub')}>-</button>
                    <button>{this.state.quantity}</button>
                    <button onClick={this.onQuantityButtonClick.bind(this, 'add')}>+</button>
                </div>
                <div className="Product-button-div">
                    <button className="Product-button" onClick={this.onBasketAddButtonClick.bind(this)}>DODAJ DO
                        KOSZYKA
                    </button>
                </div>
                <ToolTip id={this.id} mode={this.props.mode}/>
            </div>
        );
    }
}

class CompleteBoard extends React.Component {
    render() {
        return (
            <div className="Complete-board">
                <Filter/>
                <Board/>
            </div>
        );
    }
}

export default CompleteBoard;